package dev

import (
	"gitlab.com/pjdowner/ble"
	"gitlab.com/pjdowner/ble/linux"
)

// DefaultDevice ...
func DefaultDevice() (d ble.Device, err error) {
	return linux.NewDevice()
}
