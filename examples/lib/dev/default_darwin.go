package dev

import (
	"gitlab.com/pjdowner/ble"
	"gitlab.com/pjdowner/ble/darwin"
)

// DefaultDevice ...
func DefaultDevice() (d ble.Device, err error) {
	return darwin.NewDevice()
}
